import junit.framework.TestCase;
import org.junit.Test;

import java.awt.*;

public class MyDCTest extends TestCase {
    MyDC myDC = new MyDC();

    public void testCalcSingle(){//测试两个操作数之间的运算是否正确
        Throwable tx = null;
        assertEquals("6",myDC.calcSingle("12/3","2","+"));
        assertEquals("-1",myDC.calcSingle("3","4","-"));
        assertEquals("22",myDC.calcSingle("11","2","*"));
        assertEquals("11/2",myDC.calcSingle("11","2","÷"));
    }

    public void testCalcSingleDivideZero() throws ArithmeticException{
        try{
            myDC.calcSingle("2", "0", "÷");
            fail("Expected an ArithmeticException to be thrown");
        }catch (ArithmeticException e){

        }

    }
    @Test
    public void testCalculate() throws ExprFormatException {//测试后缀表达式的运算是否正确
        assertEquals("35100/17",myDC.calculate("75 2 1 26 * 9 * 42 71 96 - + ÷ * *"));
        assertEquals("24/23",myDC.calculate("72 35 35 66 3 + - - ÷"));
        assertEquals("30",myDC.calculate("10 31 - 51 + "));
        try{
            new MyDC().calculate("1 2 5 -");
            fail();
        }catch (ExprFormatException e){
            assertEquals(e.getMessage(),"Expression Format Error");
        }
    }
}