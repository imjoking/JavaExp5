import junit.framework.TestCase;
import org.junit.Test;

public class MyBCTest extends TestCase {
    MyBC MyBC = new MyBC();
    MyBC MyBC1 = new MyBC();
    MyBC MyBC2 = new MyBC();
    MyBC MyBC3 = new MyBC();
    @Test
    public void testJudgeValue() {//测试Judgevalue方法
        assertEquals(1,MyBC.judgeValue("("));
        assertEquals(2,MyBC.judgeValue("+"));
        assertEquals(2,MyBC.judgeValue("-"));
        assertEquals(3,MyBC.judgeValue("*"));
        assertEquals(3,MyBC.judgeValue("÷"));
        assertEquals(4,MyBC.judgeValue(")"));
        assertEquals(0,MyBC.judgeValue("g"));
        assertEquals(0,MyBC.judgeValue("."));
    }
    @Test
    public void testGetEquation() throws ExprFormatException {//测试中缀转后缀
        assertEquals("6 65 79 62 ÷ - * ",MyBC.getEquation("6 * ( 65 - 79 ÷ 62 )"));
        assertEquals("63 9/5 2 5 26 32 + + - ÷ + ",MyBC1.getEquation("63 + 9/5 ÷ ( 2 - ( 5 + ( 26 + 32 ) ) )"));
        assertEquals("65 5/7 - 40 - 48 * 23 - 52 33 12 * 87 ÷ 87 ÷ - + ", MyBC2.getEquation("( 65 - 5/7 - 40 ) * 48 - 23 + ( 52 - ( 33 * 12 ÷ 87 ) ÷ 87 )"));
        assertEquals("1 ",MyBC3.getEquation("( ( ( ( ( 1 ) ) ) ) )"));
        try {
            MyBC.getEquation("( 1 + 2 ) * 3 )");
            fail();
        }catch (ExprFormatException e){
            assertEquals(e.getMessage(),"Missing Left Brackets");
        }
        try {
            MyBC.getEquation("( ( 1 + 2 ) * 3");
            fail();
        }catch (ExprFormatException e){
            assertEquals(e.getMessage(),"Missing Right Brackets");
        }
    }
}