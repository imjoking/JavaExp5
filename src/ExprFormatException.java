public class ExprFormatException extends Exception {
    private String message;
    public ExprFormatException(String msg){
        this.message=msg;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
