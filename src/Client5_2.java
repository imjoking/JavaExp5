import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Scanner;
import java.net.*;
public class Client5_2 {
    public static void main(String[] args) {
        //客户端让用户输入中缀表达式，然后把中缀表达式调用MyBC.java的功能转化为后缀表达式，把后缀表达式通过网络发送给服务器
        Scanner scanner = new Scanner(System.in);
        Socket mysocket;
        DataInputStream in = null;
        DataOutputStream out = null;
        try{
            mysocket = new Socket("127.0.0.1",2010);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            System.out.println("请输入要计算的题目：");
            String formula = scanner.nextLine();
            String regex = ".*[^0-9|+|\\-|*|÷|(|)|\\s|/].*";
            if(formula.matches(regex)){
                System.out.println("输入了非法字符");
                System.exit(1);
            }
            String output = "";
            MyBC myBC = new MyBC();
            try{
                output = myBC.getEquation(formula);
            }catch (ExprFormatException e){
                System.out.println(e.getMessage());
                System.exit(1);
            }
            //调用MyBC得到后缀表达式
            out.writeUTF(output);
            String s = in.readUTF();
            System.out.println("客户收到服务器的回答："+s);
        }catch (Exception e){
            System.out.println("服务器已断开"+e);
        }

    }
}
