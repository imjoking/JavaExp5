import java.security.*;
public class DigestPass{
    public static String digPass(String plain) throws Exception{
        MessageDigest m=MessageDigest.getInstance("MD5");
        m.update(plain.getBytes("UTF8"));
        byte s[ ]=m.digest( );
        String result="";
        for (int i=0; i<s.length; i++){
            result+=Integer.toHexString((0x000000ff & s[i]) |
                    0xffffff00).substring(6);
        }
        return result;
    }
}