import java.util.EmptyStackException;
import java.util.Stack;
import java.util.StringTokenizer;

public class MyBC extends calcArithmatic{
    private Stack<String> OpStack;
    private String output="";

    public MyBC(){
        OpStack = new Stack<String>();
    }
    private void Shunt(String expr)throws ExprFormatException{
        String token;
        StringTokenizer tokenizer = new StringTokenizer(expr);
        while (tokenizer.hasMoreTokens()){
            token=tokenizer.nextToken();
            if (isOperator(token)){
                if (token.equals(")")){
                    try{
                        while (!OpStack.peek().equals("(")) {
                            output = output.concat(OpStack.pop() + " ");
                        }
                        OpStack.pop();
                    }catch (EmptyStackException e){
                        throw new ExprFormatException("Missing Left Brackets");
                    }
                }
                else if (!OpStack.empty()){
                    if(judgeValue(token)>judgeValue(OpStack.peek()) && !token.equals(")") ||
                            token.equals("(")) {
                        OpStack.push(token);
                    }
                    else {
                        while (!OpStack.empty() && judgeValue(token)<=judgeValue(OpStack.peek())){
                            output=output.concat(OpStack.pop()+" ");
                        }
                        OpStack.push(token);
                    }
                } else {
                    OpStack.push(token);
                }
            } else {
                output=output.concat(token+" ");
            }
        }
        while (!OpStack.empty()){
            if (OpStack.peek().equals("(")){
                throw new ExprFormatException("Missing Right Brackets");
            }
            output=output.concat(OpStack.pop()+" ");
        }
    }
    public int judgeValue(String str){
        int value;
        switch(str){
            case "(":
                value=1;
                break;
            case "+":
            case "-":
                value=2;
                break;
            case "*":
            case "÷":
                value=3;
                break;
            case ")":
                value=4;
                break;
            default:
                value=0;
        }
        return value;
    }
    public String getEquation(String str) throws ExprFormatException{
        Shunt(str);
        return output;
    }
}