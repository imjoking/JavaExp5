import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;

public class Server5_6 {
    public static void main(String[] args) {
        String mode = "AES";
        ServerSocket serverForClient = null;
        Socket socketOnServer = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try{
            serverForClient = new ServerSocket(2010);
        }catch (IOException e1){
            System.out.println(e1);
        }
        String result;
        try{
            System.out.println("等待客户呼叫：");
            socketOnServer = serverForClient.accept();
            out = new DataOutputStream(socketOnServer.getOutputStream());
            in = new DataInputStream(socketOnServer.getInputStream());

            //接收加密后的后缀表达式
            String cformula = in.readUTF();
            byte cipher[] = H_B.parseHexStr2Byte(cformula);

            //接收Client端公钥
            String push = in.readUTF();
            byte np[] = H_B.parseHexStr2Byte(push);
            KeyFactory kf = KeyFactory.getInstance("DH");
            PublicKey ClientPub = kf.generatePublic(new X509EncodedKeySpec(np));

            //创建服务器DH算法公、私钥
            KeyPair keyPair = Key_DH5_6.createPubAndPriKey();
            PublicKey pbk = keyPair.getPublic();//Server公钥
            PrivateKey prk = keyPair.getPrivate();//Server私钥

            //将服务器公钥传给Client端
            byte cpbk[] = pbk.getEncoded();
            String CpubKey = B_H.parseByte2HexStr(cpbk);
            out.writeUTF(CpubKey);
            Thread.sleep(1000);

            //生成共享信息，并生成AES密钥
            SecretKeySpec key = KeyAgree5_6.createKey(ClientPub,prk);

            String k = in.readUTF();//读取加密后密钥
            byte[] encryptKey = H_B.parseHexStr2Byte(k);

            //对加密后密钥进行解密
            Cipher cp = Cipher.getInstance(mode);
            cp.init(Cipher.DECRYPT_MODE,key);
            byte decryptKey [] = cp.doFinal(encryptKey);

            //对密文进行解密
            SecretKeySpec plainkey=new  SecretKeySpec(decryptKey,mode);
            cp.init(Cipher.DECRYPT_MODE, plainkey);
            byte []plain=cp.doFinal(cipher);

            //计算后缀表达式结果
            String formula = new String(plain);
            MyDC myDC = new MyDC();
            try{
                result = myDC.calculate(formula);
                //后缀表达式formula调用MyDC进行求值
            }catch (ExprFormatException e){
                result = e.getMessage();
            }catch (ArithmeticException e0){
                result = "Divide Zero Error";
            }
            //将计算结果传给Client端
            out.writeUTF(result);
        }catch (Exception e){
            System.out.println("客户已断开"+e);
        }
    }
}
